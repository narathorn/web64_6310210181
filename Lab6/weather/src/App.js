import logo from './logo.svg';
import './App.css';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Box, AppBar, Toolbar, Typography, Button } from '@mui/material';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import { useEffect, useState } from 'react';
import axios from 'axios';
import YouTube from 'react-youtube';

function App() {
  // 287091431c06aa75e8a8b2c1aac51e2f
  const [temperature, setTemperature] = useState();

  const weatherAPIBaseUrl =
    "http://api.openweathermap.org/data/2.5/weather?";

  const city = "Songkhla";
  const apikey = "287091431c06aa75e8a8b2c1aac51e2f";

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '287091431c06aa75e8a8b2c1aac51e2f',
    lat: '6.8333',
    lon: '100.6667',
    lang: 'th',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  useEffect(() => {
    setTemperature("processing...");

    axios.get(weatherAPIBaseUrl + "q=" + city + "&appid=" + apikey).then((response) => {
      let data = response.data;
      let temp = data.main.temp - 273;
      setTemperature(temp.toFixed(2));

    })

  }
    , []);


  return (
    <Box sx={{ flexGrow: 1, width: "100%" }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">
            Weather App
          </Typography>
        </Toolbar>
      </AppBar>

      <Box sx={{ justifyContent: "center", marginTop: "20px", width: "100%", display: "flex" }}>
        <Typography variant="h4">
          อากาศหาดใหญ่วันนี้
        </Typography>
      </Box>

      <Box sx={{ justifyContent: "center", marginTop: "20px", width: "100%", display: "flex" }}>
        <Card sx={{ minWidth: 275 }}>
          <CardContent>
            <Typography sx={{ fontSize: 14 }} color="text.secondary">
              อุณหภูมิหาดใหญ่วันนี้คือ
            </Typography>

            <Typography variant="h6" component="div">
              {temperature}
            </Typography>

            <Typography variant="h6" component="div">
              องศาเซลเซียส
            </Typography>

          </CardContent>
        </Card>
        <ReactWeather
          isLoading={isLoading}
          errorMessage={errorMessage}
          data={data}
          lang="th"
          locationLabel="Songkhla"
          unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
          showForecast
        />
        
        <YouTube videoId="VzIe5oBSp4A" height= '390' width= '640'/>
       
      </Box>
    </Box >
    
  );
}

export default App;
