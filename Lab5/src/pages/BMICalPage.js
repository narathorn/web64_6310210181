

import BMIResult from '../components/BMIResult';
import { useState } from "react";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import {Typography, Box, Container} from '@mui/material';

function BMICalPage() {

    const [name, setName] = useState();
    const [bmiResult, setBmiResult] = useState(0);
    const [translateResult, setTranslateResult] = useState();

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h * h);
        setBmiResult(bmi);
        if (bmi > 25) {
            setTranslateResult("เด็กอ้วน")
        } else {
            setTranslateResult("เด็กแห้ง")
        }
    }

    return (
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop: "10px" }}>
                <Grid item xs={12} >
                    <Typography variant="h4">
                        ยินดีตอนรับสู่เว็บคำนวณ BMI
                    </Typography>
                </Grid>
                <Grid item xs={6} >
                    <Box sx={{ textAlign: 'left' }}>

                        คุณชื่อ: <input type="text"
                            value={name}
                            onChange={
                                (e) => {
                                    setName(e.target.value);
                                }
                            } /> <br /> <br />
                        ส่วนสูง: <input type="text"
                            value={height}
                            onChange={
                                (e) => {
                                    setHeight(e.target.value);
                                }

                            } /> <br /> <br />
                        น้ำหนัก: <input type="text"
                            value={weight}
                            onChange={
                                (e) => {
                                    setWeight(e.target.value);
                                }

                            } /> <br /> <br />

                        <Button variant="contained" onClick={() => { calculateBMI() }} >calculate</Button>
                    </Box>
                </Grid>


                <Grid item xs={6} >
                    {bmiResult != 0 &&
                        <div>
                            <BMIResult
                                name={name}
                                bmi={bmiResult}
                                result={translateResult}

                            />
                        </div>
                    }
                </Grid>

            </Grid>
        </Container>


    );
}

export default BMICalPage;
/*
<div align="left">
            <div align="center">
                ยินดีตอนรับสู่เว็บคำนวณ BMI
                <hr />
                คุณชื่อ: <input type="text"
                    value={name}
                    onChange={
                        (e) => {
                            setName(e.target.value);
                        }
                    } /> <br />
                ส่วนสูง: <input type="text"
                    value={height}
                    onChange={
                        (e) => {
                            setHeight(e.target.value);
                        }

                    } /> <br />
                น้ำหนัก: <input type="text"
                    value={weight}
                    onChange={
                        (e) => {
                            setWeight(e.target.value);
                        }

                    } /> <br />

                <Button variant="contained" onClick={() => { calculateBMI() }} >calculate</Button>
                <hr />

                {bmiResult != 0 &&
                    <div>
                        <BMIResult
                            name={name}
                            bmi={bmiResult}
                            result={translateResult}

                        />
                    </div>
                }
            </div>
        </div>
*/