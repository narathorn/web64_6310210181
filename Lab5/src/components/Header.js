
import { Link } from "react-router-dom";
// function Header(){

//     return(
//         <div align="left">
//             ยินดีตอนรับสู่เว็บคำนวณ BMI : &nbsp;&nbsp;
//             <Link to = "/">เครื่องคิดเลข</Link>
//             &nbsp;&nbsp;&nbsp;
//             <Link to = "/about">ผู้จัดทำ</Link>
//             &nbsp;&nbsp;&nbsp;
//             <Link to = "/lucky">LuckyNumber</Link>
//         <hr />
//         </div>
//         );
// }

// export default Header;
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" >
                        ยินดีตอนรับสู่เว็บคำนวณ BMI :
                    </Typography>
                    &nbsp;&nbsp;&nbsp;

                    <Link to="/">
                        <Typography variant="boby1" color="#6169e8" >
                            เครื่องคิดเลข
                        </Typography>
                    </Link>

                    &nbsp;&nbsp;&nbsp;

                    <Link to="/about">
                        <Typography variant="boby1" color="#6169e8" >
                            ผู้จัดทำ
                        </Typography>
                    </Link>

                    &nbsp;&nbsp;&nbsp;

                    <Link to="/lucky">
                        <Typography variant="boby1" color="#6169e8" >
                            LuckyNumber
                        </Typography>
                    </Link>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default Header;
