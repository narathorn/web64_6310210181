import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

// const AboutUs = () =>{
function AboutUs (props) {
    return(
        <Box sx ={{width: '60%'}}>
            <Paper elevation={3}>
                        <h2> จัดทำโดย: {props.name} </h2>
                        <h3> ติดต่อได้ที่ร้าน {props.address} </h3>
                        <h3> บ้านอยู่ที่ {props.province} </h3>

            </Paper>
        </Box>
    );
}

export default AboutUs;