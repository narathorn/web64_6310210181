import { useEffect, useState } from "react";
import ResultLuckyNumber from '../components/ResultLuckyNumber';

function LuckyNumber() {

    const [number, setNumber] = useState("");
    const [Checkresult, setCheckresult] = useState("");
    const [randomnumber, setRandomnumber] = useState(0);

    const [result, setResult] = useState(0);

    useEffect(() => { 
        setRandomnumber(Math.floor((Math.random()*100)));
    });

    function CheckNum() {
        let n = parseInt(number);
        let nr = parseInt(randomnumber);
        setResult(nr);
        if (n == nr) {
            setCheckresult("ถูก")
        } else {
            setCheckresult("ผิด")
        }
    }

    return (
        <div align="left">
            <div align="center">
                <hr />
                กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <input type="text" 
                value={number}
                onChange={
                    (e) => {
                        setNumber(e.target.value);
                    }
                } /> <br />
               


                <button onClick={() => { CheckNum() }}> ทาย </button>

                <hr />
                 { result != 0 && 
                    <div>
                        <ResultLuckyNumber

                            number={number}
                            result={Checkresult}
                            luckynumber={randomnumber}

                        />
                    </div>
                }
            </div>
        </div>


    );
}

export default LuckyNumber;