

import BMIResult from '../components/BMIResult';
import { useState } from "react";

function BMICalPage() {

    const [name, setName] = useState();
    const [bmiResult, setBmiResult] = useState(0);
    const [translateResult, setTranslateResult] = useState();

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h * h);
        setBmiResult(bmi);
        if (bmi > 25) {
            setTranslateResult("เด็กอ้วน")
        } else {
            setTranslateResult("เด็กแห้ง")
        }
    }

    return (
        <div align="left">
            <div align="center">
                ยินดีตอนรับสู่เว็บคำนวณ BMI
                <hr />
                คุณชื่อ: <input type="text"
                    value={name}
                    onChange={
                        (e) => {
                            setName(e.target.value);
                        }
                    } /> <br />
                ส่วนสูง: <input type="text"
                    value={height}
                    onChange={
                        (e) => {
                            setHeight(e.target.value);
                        }

                    } /> <br />
                น้ำหนัก: <input type="text"
                    value={weight}
                    onChange={
                        (e) => {
                            setWeight(e.target.value);
                        }

                    } /> <br />

                <button onClick={() => { calculateBMI() }}> calculate </button>

                <hr />
                {bmiResult != 0 &&
                    <div>
                        <BMIResult
                            name={name}
                            bmi={bmiResult}
                            result={translateResult}

                        />
                    </div>
                }
            </div>
        </div>


    );
}

export default BMICalPage;