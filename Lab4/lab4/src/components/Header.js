
import {Link} from "react-router-dom";
function Header(){

    return(
        <div align="left">
            ยินดีตอนรับสู่เว็บคำนวณ BMI : &nbsp;&nbsp;
            <Link to = "/">เครื่องคิดเลข</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to = "/about">ผู้จัดทำ</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to = "/lucky">LuckyNumber</Link>
        <hr />
        </div>
        );
}

export default Header;