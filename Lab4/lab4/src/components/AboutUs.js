
// const AboutUs = () =>{
function AboutUs (props) {
    return(
        <div>
                    <h2> จัดทำโดย: {props.name} </h2>
                    <h3> ติดต่อได้ที่ร้าน {props.address} </h3>
                    <h3> บ้านอยู่ที่ {props.province} </h3>

        </div>
    );
}

export default AboutUs;