const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'seller_admin',      
    password: 'seller_admin',
    database: 'SellerSystem'
})

connection.connect()

const express = require('express')
const app = express()
const port = 2000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

//body
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Seller WHERE Username = '${username}'`
    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username": rows[0].Username,
                        "user_id": rows[0].SellerID
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: '1d' })
                    res.send(token)
                } else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})


app.post("/post", authenticateToken, (req, res) => {
    let user_profile = req.user
    console.log(user_profile)
    let seller_id = req.user.user_id
    let product_id = req.query.product_id

    let query = `INSERT INTO Post 
                     (SellerID, ProductID,  PostTime)
                    VALUES( '${seller_id}',
                            '${product_id}',
                            NOW())`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding event succesful"
            })
        }
    });
})

app.post("/register_seller", (req, res) => {
    let seller_name = req.query.seller_name
    let seller_surname = req.query.seller_surname
    let seller_username = req.query.seller_username
    let seller_password = req.query.seller_password

    bcrypt.hash(seller_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Seller
                    (SellerName, SellerSurname, Username, Password)
                    VALUES ('${seller_name}', 
                            '${seller_surname}',
                            '${seller_username}',
                            '${hash}')`
        console.log(query)

        connection.query(query, (err, rows) => {
            if (err) {
                res.json({
                    "status": "400",
                    "message": "Error inserting data into db"
                })
            } else {
                res.json({
                    "status": "200",
                    "message": "Adding new user succesful"
                })
            }
        });
    })
})

app.get("/list", (req, res) => {
    let query = "SELECT * FROM Seller"
    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error querying from running db"
            })
        } else {
            res.json(rows)
        }
    })
})

app.listen(port, () => {
    console.log('Now starting Running System Backend ' + port)
})