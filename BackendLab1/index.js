const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/triangle', (req, res) => {
    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)
    var result = {}

    if (!isNaN(base) && !isNaN(height)) {
        let area = 0.5 * base * height

        result = {
            "status": 200,
            "area": area
        }
    }
    else {
        result = {
            "status": 400,
            "message": "Base or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))
})

app.post('/score', (req, res) => {
    let name = req.query.name
    let score = parseFloat(req.query.score)
    var result = {}

    if (!isNaN(score) && score >= 0 && score <= 100) {
        let grade = ''

        if (score >= 80) {
            grade = 'A'
        }
        else if (score >= 70) {
            grade = 'B'
        }
        else if (score >= 60) {
            grade = 'C'
        }
        else if (score >= 50) {
            grade = 'D'
        }
        else {
            grade = 'E'
        }

        result = {
            "status": 200,
            "name": name,
            "grade": grade
        }
    }
    else if (!isNaN(score) && !(score >= 0 && score <= 100)) {
        result = {
            "status": 400,
            "message": "Score is not between 0-100"
        }
    }
    else {
        result = {
            "status": 400,
            "message": "Score is not a number"
        }
    }
    res.send(JSON.stringify(result))
})

app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if (!isNaN(weight) && !isNaN(height)) {
        let bmi = weight / (height * height)

        result = {
            "status": 200,
            "bmi": bmi
        }
    }
    else {
        result = {
            "status": 400,
            "message": "Weight or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))

})

app.get('/hello', (req, res) => {
    res.send('Sawasdee ' + req.query.name)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})