import './Song.css';

function Song() {

    return (
        <div id="BGS">
            <h2>Hook</h2>

            <h4>
                ไม่ได้อยากเป็นแฟนคลับ<br />

                แต่อยากเป็นแฟนครับ<br />

                เธอรู้ตัวบ้างสักที<br />

                ไม่ได้อยากเป็นแฟนคลับ<br />

                แต่อยากเป็นมากกว่านี้<br />

                ฉันต้องทำอย่างไร ตอบทำใจแล้วกัน<br />
            </h4>
        </div>
    );
}
export default Song;