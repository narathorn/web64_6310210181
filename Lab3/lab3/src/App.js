import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Song from './components/Song';

function App() {
  return (
    <div className="App">
      <Header />
      <header className="App-header">
        <p>
          My favorite song
        </p>
        <a
         href="https://www.youtube.com/watch?v=yE-0eeQjYV8"
        >
         แฟนครับ - Pearpilincys I MV
        </a>
      </header>
      <Song />
      <Footer />

    </div>
    
  );
}

export default App;
